package models;

import database.Control;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import objects.Employee;

/**
 *
 * @author Paucar
 */
public class EmployeesModel implements Control {

    private String searchActiveEmployee;
    private String searchFiredEmployee;

    public boolean saveNewEmployeeToDB(Employee employee) {
        String sql = "CALL sp_registrar_empleado(?, ?, ?, ?, ?, ?, ?)";
        try {
            //_dni int(8), _ap_pat varchar(45), _ap_mat varchar(45), _nombres varchar(45), _correo varchar(45), _telefono int(9), _cargo varchar(45)
            control.ps = control.con.prepareStatement(sql);
            control.ps.setInt(1, Integer.parseInt(employee.getDni() != null ? employee.getDni() : "00000000"));
            control.ps.setString(2, employee.getLastName());
            control.ps.setString(3, employee.getLastNameMother() != null ? employee.getLastNameMother() : "null");
            control.ps.setString(4, employee.getNames());
            control.ps.setString(5, employee.getEmail() != null ? employee.getEmail() : "null");
            control.ps.setInt(6, Integer.parseInt(employee.getPhone() != null ? employee.getPhone() : "0"));
            control.ps.setString(7, employee.getPosition());

            control.ps.execute();
            return true;
        } catch (SQLException ex) {
            System.err.println(ex);
            return false;
        }
    }

    public ArrayList<Employee> getActiveEmployeesFromDB() {
        ArrayList<Employee> employees;
        ArrayList<String> alEm;
        try {
            employees = new ArrayList<>();
            String sql = "SELECT * FROM v_empleados_activos WHERE Paterno LIKE '" + searchActiveEmployee + "%' OR DNI LIKE '" + this.searchActiveEmployee + "%';";
            int numColumns = 9;
            ResultSet rs = control.ReturnRow(sql);
            while (rs.next()) {
                alEm = new ArrayList<>();
                for (int i = 0; i < numColumns; i++) {
                    alEm.add(rs.getString((i + 1)));
                }
                employees.add(new Employee(alEm));
            }
            return employees;
        } catch (SQLException ex) {
            return null;
        }
    }

    public void setSearchActiveEmployee(String searchActiveEmployee) {
        this.searchActiveEmployee = searchActiveEmployee;
    }

    public void setSearchFiredEmployee(String searchFiredEmployee) {
        this.searchFiredEmployee = searchFiredEmployee;
    }

    public ArrayList<String> getPositionsFromDB() {
        String sql = "SELECT cargo FROM cargos;";
        return control.getData(sql);
    }

}
