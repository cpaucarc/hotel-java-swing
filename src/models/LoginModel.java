package models;

import database.Control;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import objects.LastUser;

/**
 *
 * @author Paucar
 */
public class LoginModel implements Control {

    private String username;
    private String password;

    private int userId;
    private int employeeId;
    private int employeeAvailableStatus;

    private File lastLoggedUser = new File("src/assets/logs/LastLoggedUser.properties");
    private Properties prop;

    public LoginModel() {

    }

    public LoginModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public boolean signIn() {
        String sql = "SELECT * FROM usuarios WHERE usuario = '" + this.username + "' AND contrasena = md5('" + this.password + "');";
        try {
            boolean existUser = control.existInDB(sql);

            if (existUser) {
                String[] userData = control.getRowData(sql, 5);

                this.userId = Integer.parseInt(userData[0]);
                this.employeeId = Integer.parseInt(userData[3]);
                this.employeeAvailableStatus = Integer.parseInt(userData[4]);
                return true;
            } else {
                this.userId = 0;
                this.employeeAvailableStatus = 0;
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void saveLastLoggedUser() {

        prop = new Properties();

        try {
            InputStream in = new FileInputStream(lastLoggedUser);
            prop.load(in);

            prop.setProperty("username", this.username);
            prop.setProperty("password", this.password);

            prop.store(new FileOutputStream(lastLoggedUser), null);

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public LastUser getLastLoggedUser() {

        LastUser lastUser;

        try {
            try (InputStream inputStream = new FileInputStream(this.lastLoggedUser)) {
                prop = new Properties();
                prop.load(inputStream);

                lastUser = new LastUser();

                String _username = prop.getProperty("username");
                String _password = prop.getProperty("password");

                if (_username == null) {
                    _username = "";
                }

                if (_password == null) {
                    _password = "";
                }

                lastUser.setUsername(_username);
                lastUser.setPassword(_password);

                return lastUser;
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
            return null;
        } catch (IOException ex) {
            System.out.println("I/O Error");
            return null;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public boolean isEmployeeAvailableStatus() {
        return employeeAvailableStatus == 1;
    }
}
