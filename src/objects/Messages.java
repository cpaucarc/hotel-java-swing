package objects;

import javax.swing.JOptionPane;

/**
 *
 * @author Paucar
 */
public final class Messages {

    public void info(String text) {
        JOptionPane.showMessageDialog(null, text, "Info", JOptionPane.INFORMATION_MESSAGE);
    }

    public void warn(String text) {
        JOptionPane.showMessageDialog(null, text, "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public void error(String text) {
        JOptionPane.showMessageDialog(null, text, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public boolean question(String text) {
        int reply = JOptionPane.showConfirmDialog(null, text, "Question", JOptionPane.YES_NO_OPTION);
        return reply == 0;
    }
}
