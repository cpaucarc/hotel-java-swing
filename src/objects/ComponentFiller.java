package objects;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import ui.TableModel;

/**
 *
 * @author Paucar
 */
public final class ComponentFiller {

    public void clearTable(TableModel model) {
        while (model.getRowCount() > 0) {
            model.removeRow(0);
        }
    }

    public void fillTable(TableModel model) {
        clearTable(model);
    }

    public void fillCombo(JComboBox combo, ArrayList<String> list) {
        combo.removeAllItems();
        combo.addItem("Select...");
        list.forEach(item -> {
            combo.addItem(item);
        });
        if (combo.getItemCount() == 1) {
            combo.removeAllItems();
            combo.addItem("No data");
        }
        combo.setSelectedIndex(0);
    }

    public void fillCombo(JComboBox combo, String[] list) {
        combo.removeAllItems();
        combo.addItem("Select...");
        for (String item : list) {
            combo.addItem(item);
        }
        if (combo.getItemCount() == 1) {
            combo.removeAllItems();
            combo.addItem("No data");
        }
        combo.setSelectedIndex(0);
    }

    public void fillList(DefaultListModel<String> model, ArrayList<String> list) {
        model.clear();
        list.forEach(item -> {
            model.addElement(item);
        });
    }

    public void fillList(DefaultListModel<String> model, String[] list) {
        model.clear();
        for (String item : list) {
            model.addElement(item);
        }
    }

}
