package objects;

import java.util.ArrayList;

/**
 *
 * @author Paucar
 */
public class Employee extends Person {

    private int id;

    private String phone;
    private String position;
    private double salary;

    public Employee() {
        super();
    }

    public Employee(ArrayList<String> list) {
        super(list.get(1), list.get(2), list.get(3), list.get(4), list.get(5));
        this.id = Integer.parseInt(list.get(0));
        this.phone = list.get(6);
        this.position = list.get(7);
        this.salary = Double.parseDouble(list.get(8));
    }

    public Employee(int id, String dni, String lastName, String lastNameMother, String names, String email, String phone, String position, double salary) {
        super(dni, lastName, lastNameMother, names, email);
        this.id = id;
        this.phone = phone;
        this.position = position;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String telefono) {
        this.phone = telefono;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
