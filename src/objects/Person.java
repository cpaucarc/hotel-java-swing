package objects;

/**
 *
 * @author Paucar
 */
public class Person {

    private String dni;             //null
    private String lastName;
    private String lastNameMother;  //null
    private String names;
    private String email;           //null

    public Person() {
    }

    public Person(String dni, String lastName, String lastNameMother, String names, String email) {
        this.dni = dni;
        this.lastName = lastName;
        this.lastNameMother = lastNameMother;
        this.names = names;
        this.email = email;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastNameMother() {
        return lastNameMother;
    }

    public void setLastNameMother(String lastNameMother) {
        this.lastNameMother = lastNameMother;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        if (lastNameMother == null) {
            return lastName + " " + names;
        } else {
            return lastName + " " + lastNameMother + " " + names;
        }
    }
}
