package views.panels;

public class ReservationsPanel extends javax.swing.JPanel {

    public ReservationsPanel() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel14 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txCustomerDNIReservation = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txCustomerNamesReservation = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        cbRoomTypeReservation = new javax.swing.JComboBox<>();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        dcEntryDateReservation = new com.toedter.calendar.JDateChooser();
        dcDepartureDateReservation = new com.toedter.calendar.JDateChooser();
        cbRoomNumberReservation = new javax.swing.JComboBox<>();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txCustomerEmailReservation = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txCustomerLastNameReservation = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txCustomerLastNameMotherReservation = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        cbCustomerProvinceReservation = new javax.swing.JComboBox<>();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        cbCustomerCountryReservation = new javax.swing.JComboBox<>();
        jLabel31 = new javax.swing.JLabel();
        cbCustomerStateReservation = new javax.swing.JComboBox<>();
        btEditReservation = new javax.swing.JButton();
        btSaveReservation = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel14 = new javax.swing.JPanel();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(20, 0), new java.awt.Dimension(25, 0), new java.awt.Dimension(20, 32767));
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbOnHoldReservations = new javax.swing.JTable();
        txSearchOnHoldReservations = new javax.swing.JTextField();
        btSearchOnHoldReservations = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tbCancelledReservations = new javax.swing.JTable();
        txSearchCancelledReservation = new javax.swing.JTextField();
        btSearchCancelledReservations = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Roboto Medium", 0, 17)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(30, 30, 30));
        jLabel14.setText("Reservations");
        jLabel14.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 0, 10, 0));
        jLabel14.setOpaque(true);
        add(jLabel14, java.awt.BorderLayout.PAGE_START);

        jScrollPane9.setBackground(new java.awt.Color(248, 248, 250));
        jScrollPane9.setBorder(null);
        jScrollPane9.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jPanel9.setBackground(new java.awt.Color(248, 249, 250));
        jPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(223, 223, 223)));
        jPanel9.setPreferredSize(new java.awt.Dimension(350, 750));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel15.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(30, 30, 30));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel15.setText(" Entry Date ");
        jPanel9.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 90, 30));

        jLabel16.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(30, 30, 30));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel16.setText(" Departure Date");
        jPanel9.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 90, 30));

        txCustomerDNIReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        txCustomerDNIReservation.setForeground(new java.awt.Color(30, 30, 30));
        jPanel9.add(txCustomerDNIReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 290, 200, 30));

        jLabel18.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(30, 30, 30));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel18.setText("Room Type");
        jPanel9.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 90, 30));

        txCustomerNamesReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        txCustomerNamesReservation.setForeground(new java.awt.Color(30, 30, 30));
        jPanel9.add(txCustomerNamesReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 410, 200, 30));

        jLabel19.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(30, 30, 30));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel19.setText("Names");
        jPanel9.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, 90, 30));

        jLabel20.setFont(new java.awt.Font("Roboto Medium", 0, 13)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(30, 30, 30));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel20.setText("DNI");
        jPanel9.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 90, 30));

        cbRoomTypeReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        cbRoomTypeReservation.setForeground(new java.awt.Color(30, 30, 30));
        cbRoomTypeReservation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel9.add(cbRoomTypeReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 170, 200, 30));

        jLabel21.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(110, 110, 110));
        jLabel21.setText("Provenance Information");
        jPanel9.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 500, -1, -1));

        jLabel22.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(110, 110, 110));
        jLabel22.setText("Date Information");
        jPanel9.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel23.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(110, 110, 110));
        jLabel23.setText("Room Information");
        jPanel9.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, -1));
        jPanel9.add(dcEntryDateReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 200, 30));
        jPanel9.add(dcDepartureDateReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 200, 30));

        cbRoomNumberReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        cbRoomNumberReservation.setForeground(new java.awt.Color(30, 30, 30));
        cbRoomNumberReservation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel9.add(cbRoomNumberReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 210, 200, 30));

        jLabel24.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(30, 30, 30));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel24.setText("Room Number");
        jPanel9.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 90, 30));

        jLabel25.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(30, 30, 30));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel25.setText("Email");
        jPanel9.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 450, 90, 30));

        txCustomerEmailReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        txCustomerEmailReservation.setForeground(new java.awt.Color(30, 30, 30));
        jPanel9.add(txCustomerEmailReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 450, 200, 30));

        jLabel26.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(30, 30, 30));
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel26.setText("Last Name");
        jPanel9.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 90, 30));

        txCustomerLastNameReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        txCustomerLastNameReservation.setForeground(new java.awt.Color(30, 30, 30));
        jPanel9.add(txCustomerLastNameReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 330, 200, 30));

        jLabel27.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(30, 30, 30));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel27.setText("Last Name Mother");
        jPanel9.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 370, 110, 30));

        txCustomerLastNameMotherReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        txCustomerLastNameMotherReservation.setForeground(new java.awt.Color(30, 30, 30));
        jPanel9.add(txCustomerLastNameMotherReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 370, 200, 30));

        jLabel28.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(110, 110, 110));
        jLabel28.setText("Customer Information");
        jPanel9.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, -1, -1));

        cbCustomerProvinceReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        cbCustomerProvinceReservation.setForeground(new java.awt.Color(30, 30, 30));
        cbCustomerProvinceReservation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel9.add(cbCustomerProvinceReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 610, 200, 30));

        jLabel29.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(30, 30, 30));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel29.setText("Province");
        jPanel9.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 610, 90, 30));

        jLabel30.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(30, 30, 30));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel30.setText("Country");
        jPanel9.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 530, 90, 30));

        cbCustomerCountryReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        cbCustomerCountryReservation.setForeground(new java.awt.Color(30, 30, 30));
        cbCustomerCountryReservation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel9.add(cbCustomerCountryReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 530, 200, 30));

        jLabel31.setFont(new java.awt.Font("Roboto Medium", 0, 12)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(30, 30, 30));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel31.setText("State");
        jPanel9.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 570, 90, 30));

        cbCustomerStateReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        cbCustomerStateReservation.setForeground(new java.awt.Color(30, 30, 30));
        cbCustomerStateReservation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel9.add(cbCustomerStateReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 570, 200, 30));

        btEditReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        btEditReservation.setForeground(new java.awt.Color(38, 38, 38));
        btEditReservation.setText("Edit Reservation");
        btEditReservation.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel9.add(btEditReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 670, 140, 30));

        btSaveReservation.setBackground(new java.awt.Color(31, 117, 203));
        btSaveReservation.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        btSaveReservation.setForeground(new java.awt.Color(255, 255, 255));
        btSaveReservation.setText("Save Reservation");
        btSaveReservation.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel9.add(btSaveReservation, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 670, 140, 30));

        jScrollPane9.setViewportView(jPanel9);

        add(jScrollPane9, java.awt.BorderLayout.LINE_START);

        jScrollPane6.setBorder(null);
        jScrollPane6.setPreferredSize(new java.awt.Dimension(200, 397));

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setPreferredSize(new java.awt.Dimension(400, 400));
        jPanel14.setLayout(new java.awt.BorderLayout());
        jPanel14.add(filler2, java.awt.BorderLayout.LINE_START);

        jTabbedPane3.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane3.setForeground(new java.awt.Color(30, 30, 30));
        jTabbedPane3.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));

        jPanel16.setBackground(new java.awt.Color(248, 249, 250));
        jPanel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(223, 223, 223)));

        tbOnHoldReservations.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane7.setViewportView(tbOnHoldReservations);

        btSearchOnHoldReservations.setBackground(new java.awt.Color(31, 117, 203));
        btSearchOnHoldReservations.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/search_17px.png"))); // NOI18N
        btSearchOnHoldReservations.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(txSearchOnHoldReservations, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSearchOnHoldReservations, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 115, Short.MAX_VALUE)))
                .addGap(20, 20, 20))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txSearchOnHoldReservations, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSearchOnHoldReservations, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane3.addTab(" On hold", jPanel15);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));

        jPanel18.setBackground(new java.awt.Color(248, 248, 250));
        jPanel18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(234, 236, 239)));

        tbCancelledReservations.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane8.setViewportView(tbCancelledReservations);

        btSearchCancelledReservations.setBackground(new java.awt.Color(31, 117, 203));
        btSearchCancelledReservations.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/search_17px.png"))); // NOI18N
        btSearchCancelledReservations.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(txSearchCancelledReservation, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSearchCancelledReservations, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 115, Short.MAX_VALUE)))
                .addGap(20, 20, 20))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txSearchCancelledReservation, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSearchCancelledReservations, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane3.addTab(" Cancelled", jPanel17);

        jPanel14.add(jTabbedPane3, java.awt.BorderLayout.CENTER);

        jScrollPane6.setViewportView(jPanel14);

        add(jScrollPane6, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btEditReservation;
    public javax.swing.JButton btSaveReservation;
    public javax.swing.JButton btSearchCancelledReservations;
    public javax.swing.JButton btSearchOnHoldReservations;
    public javax.swing.JComboBox<String> cbCustomerCountryReservation;
    public javax.swing.JComboBox<String> cbCustomerProvinceReservation;
    public javax.swing.JComboBox<String> cbCustomerStateReservation;
    public javax.swing.JComboBox<String> cbRoomNumberReservation;
    public javax.swing.JComboBox<String> cbRoomTypeReservation;
    public com.toedter.calendar.JDateChooser dcDepartureDateReservation;
    public com.toedter.calendar.JDateChooser dcEntryDateReservation;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane3;
    public javax.swing.JTable tbCancelledReservations;
    public javax.swing.JTable tbOnHoldReservations;
    public javax.swing.JTextField txCustomerDNIReservation;
    public javax.swing.JTextField txCustomerEmailReservation;
    public javax.swing.JTextField txCustomerLastNameMotherReservation;
    public javax.swing.JTextField txCustomerLastNameReservation;
    public javax.swing.JTextField txCustomerNamesReservation;
    public javax.swing.JTextField txSearchCancelledReservation;
    public javax.swing.JTextField txSearchOnHoldReservations;
    // End of variables declaration//GEN-END:variables
}
