package views;

/**
 *
 * @author Paucar
 */
public class LoginView extends javax.swing.JFrame{

    public LoginView() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btSignIn = new javax.swing.JButton();
        btForgotPassword = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txUsername = new javax.swing.JTextField();
        txPassword = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        chKeep = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(650, 530));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(248, 249, 250));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(223, 223, 223)));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btSignIn.setBackground(new java.awt.Color(0, 122, 255));
        btSignIn.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        btSignIn.setForeground(new java.awt.Color(255, 255, 255));
        btSignIn.setText("Sign in");
        btSignIn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel2.add(btSignIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 230, 150, 30));

        btForgotPassword.setFont(new java.awt.Font("Segoe UI Semibold", 0, 13)); // NOI18N
        btForgotPassword.setForeground(new java.awt.Color(56, 117, 197));
        btForgotPassword.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btForgotPassword.setText("Forgot your password?");
        btForgotPassword.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel2.add(btForgotPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(191, 195, 160, 20));

        jLabel2.setFont(new java.awt.Font("Segoe UI Semibold", 0, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(36, 41, 46));
        jLabel2.setText("Username or email address");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, -1, 20));

        txUsername.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        txUsername.setForeground(new java.awt.Color(30, 30, 30));
        txUsername.setText("jTextField1");
        txUsername.setSelectionColor(new java.awt.Color(0, 122, 255));
        jPanel2.add(txUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, 270, 30));

        txPassword.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        txPassword.setForeground(new java.awt.Color(30, 30, 30));
        txPassword.setText("jPasswordField1");
        txPassword.setSelectionColor(new java.awt.Color(0, 122, 255));
        jPanel2.add(txPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 150, 270, 30));

        jLabel4.setFont(new java.awt.Font("Segoe UI Semibold", 0, 13)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(36, 41, 46));
        jLabel4.setText("Password");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 130, -1, 20));

        chKeep.setForeground(new java.awt.Color(36, 41, 46));
        chKeep.setText("Remember me");
        jPanel2.add(chKeep, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 195, 104, 20));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 160, 450, 310));

        jLabel3.setFont(new java.awt.Font("Berkshire Swash", 0, 28)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(36, 41, 46));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Sign in");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 450, 50));

        jLabel5.setFont(new java.awt.Font("Alegreya Medium", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/gitlab_80px.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, 90, 80));

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel btForgotPassword;
    public javax.swing.JButton btSignIn;
    public javax.swing.JCheckBox chKeep;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JPasswordField txPassword;
    public javax.swing.JTextField txUsername;
    // End of variables declaration//GEN-END:variables
}