package views;

import javax.swing.JFrame;

public class MainWindowView extends JFrame {

    public MainWindowView() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnMain = new javax.swing.JPanel();
        pnTopBar = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jButton10 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btHome = new javax.swing.JButton();
        pnSidebar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btRooms = new javax.swing.JButton();
        btCustomers = new javax.swing.JButton();
        btReservations = new javax.swing.JButton();
        btAccommodations = new javax.swing.JButton();
        btServices = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        btEmplooyes = new javax.swing.JButton();
        btReports = new javax.swing.JButton();
        cardsPanel = new javax.swing.JPanel();
        pnHome = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        pnReservations = new javax.swing.JPanel();
        pnEmployees = new javax.swing.JPanel();
        pnRooms = new javax.swing.JPanel();
        pnCustomers = new javax.swing.JPanel();
        pnAccommodations = new javax.swing.JPanel();
        pnServices = new javax.swing.JPanel();
        pnReports = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        pnMain.setLayout(new java.awt.BorderLayout());

        pnTopBar.setBackground(new java.awt.Color(248, 249, 250));
        pnTopBar.setPreferredSize(new java.awt.Dimension(870, 50));
        pnTopBar.setLayout(new java.awt.BorderLayout());

        jPanel8.setOpaque(false);
        jPanel8.setPreferredSize(new java.awt.Dimension(256, 60));

        jButton10.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        jButton10.setForeground(new java.awt.Color(30, 30, 30));
        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        jButton10.setText("Settings");
        jButton10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jButton1.setFont(new java.awt.Font("Roboto", 0, 13)); // NOI18N
        jButton1.setForeground(new java.awt.Color(30, 30, 30));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        jButton1.setText("Paucar");
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGap(0, 24, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(20, 20, 20)
                .addComponent(jButton10)
                .addGap(25, 25, 25))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(10, Short.MAX_VALUE))
        );

        pnTopBar.add(jPanel8, java.awt.BorderLayout.LINE_END);

        jPanel2.setOpaque(false);

        btHome.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        btHome.setForeground(new java.awt.Color(30, 30, 30));
        btHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/gitlab_25px.png"))); // NOI18N
        btHome.setText("Hotel");
        btHome.setToolTipText("Home");
        btHome.setBorderPainted(false);
        btHome.setContentAreaFilled(false);
        btHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btHome.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btHome.setMargin(new java.awt.Insets(2, 0, 2, 14));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(btHome, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        pnTopBar.add(jPanel2, java.awt.BorderLayout.LINE_START);

        pnMain.add(pnTopBar, java.awt.BorderLayout.PAGE_START);

        pnSidebar.setBackground(new java.awt.Color(248, 248, 250));
        pnSidebar.setPreferredSize(new java.awt.Dimension(200, 463));
        pnSidebar.setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(30, 30, 30));
        jLabel1.setText("Developed by Paucar");
        jLabel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 23, 1, 1));
        jLabel1.setIconTextGap(20);
        pnSidebar.add(jLabel1, java.awt.BorderLayout.PAGE_END);

        jScrollPane1.setBorder(null);
        jScrollPane1.setOpaque(false);

        jPanel1.setBackground(new java.awt.Color(248, 249, 250));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(110, 110, 110));
        jLabel2.setText("Features");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 10, 110, 20));

        btRooms.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btRooms.setForeground(new java.awt.Color(30, 30, 30));
        btRooms.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btRooms.setText("Rooms");
        btRooms.setBorderPainted(false);
        btRooms.setContentAreaFilled(false);
        btRooms.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btRooms.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btRooms.setIconTextGap(10);
        btRooms.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel1.add(btRooms, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 160, 30));

        btCustomers.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btCustomers.setForeground(new java.awt.Color(30, 30, 30));
        btCustomers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btCustomers.setText("Customers");
        btCustomers.setBorderPainted(false);
        btCustomers.setContentAreaFilled(false);
        btCustomers.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btCustomers.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btCustomers.setIconTextGap(10);
        btCustomers.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel1.add(btCustomers, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 160, 30));

        btReservations.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btReservations.setForeground(new java.awt.Color(30, 30, 30));
        btReservations.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btReservations.setText("Reservations");
        btReservations.setBorderPainted(false);
        btReservations.setContentAreaFilled(false);
        btReservations.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btReservations.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btReservations.setIconTextGap(10);
        btReservations.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel1.add(btReservations, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 160, 30));

        btAccommodations.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btAccommodations.setForeground(new java.awt.Color(30, 30, 30));
        btAccommodations.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btAccommodations.setText("Accommodations");
        btAccommodations.setBorderPainted(false);
        btAccommodations.setContentAreaFilled(false);
        btAccommodations.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btAccommodations.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btAccommodations.setIconTextGap(10);
        btAccommodations.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel1.add(btAccommodations, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 160, 30));

        btServices.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btServices.setForeground(new java.awt.Color(30, 30, 30));
        btServices.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btServices.setText("Services");
        btServices.setBorderPainted(false);
        btServices.setContentAreaFilled(false);
        btServices.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btServices.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btServices.setIconTextGap(10);
        btServices.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel1.add(btServices, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 160, 30));

        jPanel6.setOpaque(false);
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btEmplooyes.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btEmplooyes.setForeground(new java.awt.Color(30, 30, 30));
        btEmplooyes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btEmplooyes.setText("Employees");
        btEmplooyes.setBorderPainted(false);
        btEmplooyes.setContentAreaFilled(false);
        btEmplooyes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btEmplooyes.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btEmplooyes.setIconTextGap(10);
        btEmplooyes.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel6.add(btEmplooyes, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 160, 30));

        btReports.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btReports.setForeground(new java.awt.Color(30, 30, 30));
        btReports.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/clock_16px.png"))); // NOI18N
        btReports.setText("Reports");
        btReports.setBorderPainted(false);
        btReports.setContentAreaFilled(false);
        btReports.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btReports.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        btReports.setIconTextGap(10);
        btReports.setMargin(new java.awt.Insets(2, 0, 2, 14));
        jPanel6.add(btReports, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 160, 30));

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 240, 200, 120));

        jScrollPane1.setViewportView(jPanel1);

        pnSidebar.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pnMain.add(pnSidebar, java.awt.BorderLayout.LINE_START);

        cardsPanel.setBackground(new java.awt.Color(255, 255, 255));
        cardsPanel.setLayout(new java.awt.CardLayout());

        pnHome.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setText("Home");

        javax.swing.GroupLayout pnHomeLayout = new javax.swing.GroupLayout(pnHome);
        pnHome.setLayout(pnHomeLayout);
        pnHomeLayout.setHorizontalGroup(
            pnHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnHomeLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel3)
                .addContainerGap(721, Short.MAX_VALUE))
        );
        pnHomeLayout.setVerticalGroup(
            pnHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnHomeLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel3)
                .addContainerGap(597, Short.MAX_VALUE))
        );

        cardsPanel.add(pnHome, "pnHome");

        pnReservations.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout pnReservationsLayout = new javax.swing.GroupLayout(pnReservations);
        pnReservations.setLayout(pnReservationsLayout);
        pnReservationsLayout.setHorizontalGroup(
            pnReservationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnReservationsLayout.setVerticalGroup(
            pnReservationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnReservations, "pnReservations");

        pnEmployees.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout pnEmployeesLayout = new javax.swing.GroupLayout(pnEmployees);
        pnEmployees.setLayout(pnEmployeesLayout);
        pnEmployeesLayout.setHorizontalGroup(
            pnEmployeesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnEmployeesLayout.setVerticalGroup(
            pnEmployeesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnEmployees, "pnEmployees");

        pnRooms.setBackground(new java.awt.Color(255, 153, 153));

        javax.swing.GroupLayout pnRoomsLayout = new javax.swing.GroupLayout(pnRooms);
        pnRooms.setLayout(pnRoomsLayout);
        pnRoomsLayout.setHorizontalGroup(
            pnRoomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnRoomsLayout.setVerticalGroup(
            pnRoomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnRooms, "pnRooms");

        pnCustomers.setBackground(new java.awt.Color(255, 255, 153));

        javax.swing.GroupLayout pnCustomersLayout = new javax.swing.GroupLayout(pnCustomers);
        pnCustomers.setLayout(pnCustomersLayout);
        pnCustomersLayout.setHorizontalGroup(
            pnCustomersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnCustomersLayout.setVerticalGroup(
            pnCustomersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnCustomers, "pnCustomers");

        pnAccommodations.setBackground(new java.awt.Color(153, 204, 255));

        javax.swing.GroupLayout pnAccommodationsLayout = new javax.swing.GroupLayout(pnAccommodations);
        pnAccommodations.setLayout(pnAccommodationsLayout);
        pnAccommodationsLayout.setHorizontalGroup(
            pnAccommodationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnAccommodationsLayout.setVerticalGroup(
            pnAccommodationsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnAccommodations, "pnAccommodations");

        javax.swing.GroupLayout pnServicesLayout = new javax.swing.GroupLayout(pnServices);
        pnServices.setLayout(pnServicesLayout);
        pnServicesLayout.setHorizontalGroup(
            pnServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnServicesLayout.setVerticalGroup(
            pnServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnServices, "pnServices");

        javax.swing.GroupLayout pnReportsLayout = new javax.swing.GroupLayout(pnReports);
        pnReports.setLayout(pnReportsLayout);
        pnReportsLayout.setHorizontalGroup(
            pnReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 808, Short.MAX_VALUE)
        );
        pnReportsLayout.setVerticalGroup(
            pnReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 646, Short.MAX_VALUE)
        );

        cardsPanel.add(pnReports, "pnReports");

        pnMain.add(cardsPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnMain, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btAccommodations;
    public javax.swing.JButton btCustomers;
    public javax.swing.JButton btEmplooyes;
    public javax.swing.JButton btHome;
    public javax.swing.JButton btReports;
    public javax.swing.JButton btReservations;
    public javax.swing.JButton btRooms;
    public javax.swing.JButton btServices;
    public javax.swing.JPanel cardsPanel;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton10;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JPanel pnAccommodations;
    public javax.swing.JPanel pnCustomers;
    public javax.swing.JPanel pnEmployees;
    private javax.swing.JPanel pnHome;
    private javax.swing.JPanel pnMain;
    public javax.swing.JPanel pnReports;
    public javax.swing.JPanel pnReservations;
    public javax.swing.JPanel pnRooms;
    public javax.swing.JPanel pnServices;
    private javax.swing.JPanel pnSidebar;
    private javax.swing.JPanel pnTopBar;
    // End of variables declaration//GEN-END:variables
}
