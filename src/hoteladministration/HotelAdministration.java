package hoteladministration;

import com.formdev.flatlaf.FlatLightLaf;
import controllers.LoginController;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import views.LoginView;

/**
 *
 * @author Paucar
 */
public class HotelAdministration {

    public static void main(String[] args) {
        try {
            UIManager.put("Button.arc", 0);        //default is 6  []  -> in the project is 12
            UIManager.put("Component.arc", 0);     //default is 5  [ComboBox, Spinners]  -> in the project is 12
            UIManager.put("ProgressBar.arc", 0);    //default is 4  []  -> in the project is 5
            UIManager.put("TextComponent.arc", 0); //default is 5  []  -> in the project is 12
            UIManager.put("CheckBox.arc", 0);       //default is 4  []  -> in the project is 5
            UIManager.put("ScrollBar.width", 7);    //default is 10     -> in the project is 8
            UIManager.put("ScrollBar.showButtons", true);
            UIManager.put("Component.innerFocusWidth", 0);
            UIManager.put("Component.focusWidth", 0);
            UIManager.put("TabbedPane.selectedBackground", new Color(248,248,250));
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (UnsupportedLookAndFeelException e) {
        }

        java.awt.EventQueue.invokeLater(() -> {
            LoginController loginController = new LoginController(new LoginView());
            loginController.start();
        });
    }

}
