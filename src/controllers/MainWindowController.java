/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import views.MainWindowView;
import views.panels.EmployeePanel;

/**
 *
 * @author Paucar
 */
public class MainWindowController {

    private MainWindowView mainWindowView;

    private EmployeesController employeesController;
    private HomeController homeController;
    private ReservationsController rservationsController;

    public MainWindowController(MainWindowView mainWindowView) {
        this.mainWindowView = mainWindowView;
    }

    public void start() {
        //Controllers
            EmployeePanel employeePanel = new EmployeePanel();
        employeesController = new EmployeesController(mainWindowView, employeePanel);
        homeController = new HomeController(mainWindowView);
        rservationsController = new ReservationsController(mainWindowView);

        //Window Configuration
        mainWindowView.setExtendedState(6);
        mainWindowView.setTitle("Hotel Administrator");
        mainWindowView.setVisible(true);
    }
}
