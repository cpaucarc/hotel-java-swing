package controllers;

import controllers.listeners.reservations.OpenViewReservation;
import views.MainWindowView;

public class ReservationsController {

    private MainWindowView mainWindow;

    public ReservationsController(MainWindowView mainWindowView) {
        this.mainWindow = mainWindowView;
        setListenersToButtons();
    }

    private void setListenersToButtons() {
        mainWindow.btReservations.addActionListener(new OpenViewReservation(mainWindow));
    }
}
