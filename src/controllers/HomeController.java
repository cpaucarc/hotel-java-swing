/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.listeners.home.OpenViewHome;
import models.EmployeesModel;
import views.MainWindowView;

/**
 *
 * @author Paucar
 */
public class HomeController {

    private EmployeesModel employeesModel;
    private MainWindowView mainWindow;

    public HomeController(MainWindowView mainWindowView) {
        this.mainWindow = mainWindowView;
        employeesModel = new EmployeesModel();
        setListenersToButtons();
        setInitialSettings();
    }
    
    private void setListenersToButtons(){
        mainWindow.btHome.addActionListener(new OpenViewHome(mainWindow));
    }

    private void setInitialSettings() {
        
    }
}
