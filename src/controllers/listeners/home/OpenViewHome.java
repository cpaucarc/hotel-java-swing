/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.listeners.home;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import views.MainWindowView;

/**
 *
 * @author Paucar
 */
public class OpenViewHome extends ButtonsListenerHome{
    private CardLayout cl;

    public OpenViewHome(MainWindowView mainWindow) {
        super(mainWindow);
        cl = (CardLayout) this.mainWindow.cardsPanel.getLayout();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        cl.show(this.mainWindow.cardsPanel, "pnHome");
        this.mainWindow.repaint();
    }  
}
