package controllers.listeners.employees;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import models.EmployeesModel;
import objects.ComponentFiller;
import objects.Employee;
import ui.TableModel;
import views.MainWindowView;
import views.panels.EmployeePanel;

/**
 *
 * @author Paucar
 */
public class SearchAllActiveEmployees extends FieldListenerEmployee {

    private TableModel model;
    private ComponentFiller filler = new ComponentFiller();
    private EmployeePanel employeePanel;

    public SearchAllActiveEmployees(MainWindowView mainWindow,EmployeePanel employeePanel, TableModel model) {
        super(mainWindow);
        this.model = model;
        this.employeePanel = employeePanel;
        employeesModel = new EmployeesModel();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        employeesModel.setSearchActiveEmployee(employeePanel.txSearchActiveEmployee.getText());
        ArrayList<Employee> employees = employeesModel.getActiveEmployeesFromDB();

        filler.clearTable(model);
        employees.forEach(empl -> {
            model.addRow(new String[]{
                String.valueOf(empl.getId()),
                empl.getDni(),
                empl.getFullName(),
                empl.getEmail(),
                empl.getPhone(),
                empl.getPosition(),
                String.valueOf(empl.getSalary())
            });
        });
    }
}
