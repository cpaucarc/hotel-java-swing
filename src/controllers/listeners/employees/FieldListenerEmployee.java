/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.listeners.employees;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import models.EmployeesModel;
import objects.Employee;
import views.MainWindowView;

/**
 *
 * @author Paucar
 */
public class FieldListenerEmployee implements KeyListener {

    protected MainWindowView mainWindow;
    protected EmployeesModel employeesModel;
    protected Employee employee;

    public FieldListenerEmployee(MainWindowView mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
