package controllers.listeners.employees;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import models.EmployeesModel;
import objects.Employee;
import views.MainWindowView;

/**
 *
 * @author Paucar
 */
public class ButtonsListenerEmployee implements ActionListener {

    protected MainWindowView mainWindow;    
    protected EmployeesModel employeesModel;
    protected Employee employee;

    public ButtonsListenerEmployee(MainWindowView mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
