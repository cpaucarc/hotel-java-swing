package controllers.listeners.employees;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import views.MainWindowView;
import views.panels.EmployeePanel;

/**
 *
 * @author Paucar
 */
public class OpenViewEmployee extends ButtonsListenerEmployee {

    private CardLayout cl;
    private EmployeePanel employeePanel;

    public OpenViewEmployee(MainWindowView mainWindow, EmployeePanel employeePanel) {
        super(mainWindow);
        cl = (CardLayout) this.mainWindow.cardsPanel.getLayout();
        this.employeePanel = employeePanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setPanelIntoMainWindow();
        showView();
    }

    private void setPanelIntoMainWindow() {

        GroupLayout pnEmployeesLayout = new GroupLayout(mainWindow.pnEmployees);
        mainWindow.pnEmployees.setLayout(pnEmployeesLayout);
        
        pnEmployeesLayout.setHorizontalGroup(
            pnEmployeesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(pnEmployeesLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(this.employeePanel, GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );
        pnEmployeesLayout.setVerticalGroup(
            pnEmployeesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(pnEmployeesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(this.employeePanel, GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );
    }

    private void showView() {
        cl.show(this.mainWindow.cardsPanel, "pnEmployees");
        this.mainWindow.repaint();
    }
}
