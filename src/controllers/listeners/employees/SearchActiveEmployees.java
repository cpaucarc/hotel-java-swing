package controllers.listeners.employees;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import models.EmployeesModel;
import objects.ComponentFiller;
import objects.Employee;
import ui.TableModel;
import views.MainWindowView;
import views.panels.EmployeePanel;

/**
 *
 * @author Paucar
 */
public class SearchActiveEmployees extends ButtonsListenerEmployee{

    private TableModel model;
    private ComponentFiller filler = new ComponentFiller();
    private EmployeePanel employeePanel;

    public SearchActiveEmployees(MainWindowView mainWindow, EmployeePanel employeePanel, TableModel model) {
        super(mainWindow);
        this.model = model;
        this.employeePanel = employeePanel;
        employeesModel = new EmployeesModel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        employeesModel.setSearchActiveEmployee(employeePanel.txSearchActiveEmployee.getText());
        ArrayList<Employee> employees = employeesModel.getActiveEmployeesFromDB();

        filler.clearTable(model);
        employees.forEach(em -> {
            model.addRow(new String[]{
                String.valueOf(em.getId()),
                em.getDni(),
                em.getFullName(),
                em.getEmail(),
                em.getPhone(),
                em.getPosition(),
                String.valueOf(em.getSalary())
            });
        });
    }
}