package controllers.listeners.employees;

import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import models.EmployeesModel;
import objects.Employee;
import objects.Messages;
import views.MainWindowView;
import views.panels.EmployeePanel;

/**
 *
 * @author Paucar
 */
public class SaveNewEmployee extends ButtonsListenerEmployee {

    private Messages msg = new Messages();
    private EmployeePanel employeePanel;

    public SaveNewEmployee(MainWindowView mainWindow, EmployeePanel employeePanel) {
        super(mainWindow);
        this.employeePanel = employeePanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (allRequiredFieldAreComplete()) {
            employeesModel = new EmployeesModel();
            recoverData();
            if (employeesModel.saveNewEmployeeToDB(employee)) {
                msg.info("Succesfull");
            } else {
                msg.error("Error");
            }
        }
    }

    protected boolean allRequiredFieldAreComplete() {
        boolean reply = false;
        if (!employeePanel.txEmployeeLastname.getText().isEmpty()) {
            if (!employeePanel.txEmployeeName.getText().isEmpty()) {
                if (employeePanel.cbEmployeePosition.getSelectedIndex() > 0) {
                    reply = true;
                } else {
                    msg.warn("The employee position is missing");
                    employeePanel.cbEmployeePosition.grabFocus();
                }
            } else {
                msg.warn("The name is missing");
                employeePanel.txEmployeeName.grabFocus();
            }
        } else {
            msg.warn("The last name is missing");
            employeePanel.txEmployeeLastname.grabFocus();
        }
        return reply;
    }

    protected void recoverData() {
        employee = new Employee();
        employee.setDni(employeePanel.txEmployeeDNI.getText());
        employee.setLastName(employeePanel.txEmployeeLastname.getText());
        employee.setLastNameMother(employeePanel.txEmployeeLastNameMother.getText());
        employee.setNames(employeePanel.txEmployeeName.getText());
        employee.setEmail(employeePanel.txEmployeeEmail.getText());
        employee.setPhone(employeePanel.txEmployeePhone.getText());
        employee.setPosition(employeePanel.cbEmployeePosition.getSelectedItem().toString());
    }
}
