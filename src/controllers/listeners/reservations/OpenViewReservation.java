package controllers.listeners.reservations;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import views.MainWindowView;
import views.panels.ReservationsPanel;

public class OpenViewReservation implements ActionListener {

    private CardLayout cl;
    private MainWindowView mainWindow;
    private JPanel reservationsPanel;

    public OpenViewReservation(MainWindowView mainWindow) {
        this.mainWindow = mainWindow;
        cl = (CardLayout) this.mainWindow.cardsPanel.getLayout();
        reservationsPanel = new ReservationsPanel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setPanelIntoMainWindow();
        showView();
    }

    private void setPanelIntoMainWindow() {
        GroupLayout pnReservationsLayout = new GroupLayout(mainWindow.pnReservations);
        mainWindow.pnReservations.setLayout(pnReservationsLayout);

        pnReservationsLayout.setHorizontalGroup(
            pnReservationsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(pnReservationsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(this.reservationsPanel, GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );
        pnReservationsLayout.setVerticalGroup(
            pnReservationsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(pnReservationsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(this.reservationsPanel, GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );
    }
    
    private void showView(){
        cl.show(this.mainWindow.cardsPanel, "pnReservations");
        this.mainWindow.repaint();
    }
}
