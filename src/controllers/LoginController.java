/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import models.LoginModel;
import objects.LastUser;
import objects.Messages;
import views.LoginView;
import views.MainWindowView;

/**
 *
 * @author Paucar
 */
public class LoginController {

    private LoginView loginView;
    private LoginModel loginModel;

    
    
    public LoginController(LoginView loginView) {
        this.loginView = loginView;
        this.loginModel = new LoginModel();
        this.loginView.btSignIn.addActionListener(
                new SignInActionEvent(this.loginView, this.loginModel)
        );
    }

    public void start() {        
        loginView.setTitle("Login Window");
        loginView.setVisible(true);
        LastUser lastUser = loginModel.getLastLoggedUser();
        loginView.txUsername.setText(lastUser.getUsername());
        loginView.txPassword.setText(lastUser.getPassword());
    }

}

class SignInActionEvent implements ActionListener {

    private LoginView loginView;
    private LoginModel loginModel;
    private MainWindowController mainWindowController;
    private MainWindowView mainWindowView;
    private Messages msg = new Messages();

    public SignInActionEvent(LoginView loginView, LoginModel loginModel) {
        this.loginView = loginView;
        this.loginModel = loginModel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        loginModel.setUsername(loginView.txUsername.getText());
        loginModel.setPassword(loginView.txPassword.getText());

        boolean success = loginModel.signIn();

        if (success) {
            if (loginView.chKeep.isSelected()) {
                loginModel.saveLastLoggedUser();
            }
            mainWindowView = new MainWindowView();
            mainWindowController = new MainWindowController(mainWindowView);
            loginView.dispose();
            mainWindowController.start();
        } else {
            JOptionPane.showMessageDialog(null, "Failed");
        }
    }
}
