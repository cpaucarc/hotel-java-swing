package controllers;

import controllers.listeners.employees.OpenViewEmployee;
import controllers.listeners.employees.SaveNewEmployee;
import controllers.listeners.employees.SearchActiveEmployees;
import controllers.listeners.employees.SearchAllActiveEmployees;
import models.EmployeesModel;
import objects.ComponentFiller;
import ui.TableModel;
import ui.Tables;
import views.MainWindowView;
import views.panels.EmployeePanel;

/**
 *
 * @author Paucar
 */
public class EmployeesController {

    private EmployeesModel employeesModel;
    private EmployeePanel employeePanel;
    private MainWindowView mainWindow;

    TableModel modelActiveEmployees = new TableModel();
    ComponentFiller filler = new ComponentFiller();

    public EmployeesController(MainWindowView mainWindow, EmployeePanel employeePanel) {
        this.mainWindow = mainWindow;
        this.employeePanel = employeePanel;
        employeesModel = new EmployeesModel();
        setListenersToButtons();
        setInitialSettings();
    }

    private void setListenersToButtons() {
        mainWindow.btEmplooyes.addActionListener(new OpenViewEmployee(mainWindow, employeePanel));
        employeePanel.btSearchActiveEmployee.addActionListener(new SearchActiveEmployees(mainWindow, employeePanel, modelActiveEmployees));
        employeePanel.txSearchActiveEmployee.addKeyListener(new SearchAllActiveEmployees(mainWindow, employeePanel,modelActiveEmployees));
        employeePanel.btSaveEmployee.addActionListener(new SaveNewEmployee(mainWindow, employeePanel));
    }

    public final void setInitialSettings() {
        //Active Employees
        modelActiveEmployees.setColumnIdentifiers(new String[]{"Id", "DNI", "Employee", "Mail", "Phone", "Position", "Salary"});
        employeePanel.tbActiveEmployee.setModel(modelActiveEmployees);
        Tables.light(employeePanel.tbActiveEmployee);
        Tables.HideColumn(employeePanel.tbActiveEmployee, 0);

        //Fired Employees
        //Others
        filler.fillCombo(employeePanel.cbEmployeePosition, employeesModel.getPositionsFromDB());

        //Search fields placeholders
        this.employeePanel.txSearchActiveEmployee.putClientProperty("JTextField.placeholderText", "Search");
        this.employeePanel.txSearchFiredEmployee.putClientProperty("JTextField.placeholderText", "Search");

    }

}
