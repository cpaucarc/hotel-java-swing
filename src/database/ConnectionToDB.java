package database;

import Database.Controller;
import java.io.File;

/**
 *
 * @author Paucar
 */
public class ConnectionToDB {
    
    private final Controller control;
    private File envFile = null;

    public ConnectionToDB() {
        envFile = new File("src/database/env.properties");
        control = new Controller(envFile);
    }

    public Controller getControl() {
        return control;
    }
          
}
