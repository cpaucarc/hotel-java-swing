# Hotel Administration
### Info
- Language: **Java 8** - **Swing**
- Architectural pattern: **MVC**
- UI:	**[FlatLaf](https://github.com/JFormDesigner/FlatLaf "FlatLaf")** by JFormDesigner
######  Fonts

	1. Roboto
	2. Segoe UI
	3. Berkshire Swash


### Screenshots
![login](https://user-images.githubusercontent.com/52868996/108009666-2c7a6980-6fd1-11eb-9504-f416a553549a.png)
> Login window

![employees_window](https://user-images.githubusercontent.com/52868996/108009682-32704a80-6fd1-11eb-97b4-7a336540c4ef.png)
> Employees window

![reservations_window](https://user-images.githubusercontent.com/52868996/108009688-3734fe80-6fd1-11eb-95e7-df1dcf4713bb.png)
> Reservations window *(incomplete)*

